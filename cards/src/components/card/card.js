import react from "react";
import "./card.css";


const Card = (props) => {

    return(
        <div className="card">
            {props.image && <img src={props.image.src} className="card-img-top" alt={props.image.alt}></img>}
            <div className="card-body">
                {props.children}
            </div>
        </div>
    );
}

export default Card;