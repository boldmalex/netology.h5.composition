import react from "react";
import Card from "../card/card.js";

const Cards = ({items}) => {

    const listItems = items.map(item => <Card key = {item.id} image = {item.image}>
                                            <h5 className="card-title">{item.card.title}</h5>
                                            <p className="card-text">{item.card.text}</p>
                                            <a href={item.card.href} className="btn btn-primary">{item.card.href_text}</a>
                                        </Card>
    );

    if (items.length > 0){
        return (
            <div>
                {listItems}
            </div>
        );
    }
    else
        return null;

};

Cards.defaultProps = {
    items: []
}

export default Cards;