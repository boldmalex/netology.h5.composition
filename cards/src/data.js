import { nanoid } from "nanoid";


const data = [{id: nanoid(), image:{src:"https://cdn.pixabay.com/photo/2021/12/01/15/10/happy-new-year-6838220__340.jpg", alt:"..."}, card:{title:"Card title", text:"Some quick example text to build on the card title and make up the bulk of the card's content.", href: "...", href_text: "Go somewhere"}},
              {id: nanoid(), card:{title:"Special title treatment", text:"With supprting text below as a natural lead-in to additional content.", href: "...", href_text: "Go somewhere"}}];


export default data;            