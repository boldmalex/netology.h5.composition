import react from "react";
import Cards from "./components/cards/cards.js";
import data from "./data.js";

function App() {

  return (
    <>
      <Cards items={data}/>
    </>
  );
}

export default App;
