import react from "react";
import CustomizedList from "../customized/customized-list/customized-list.js"
import customizedLinkTypes from "../customized/customized-link/customized-link-types.js"
import "./search-area.css"
import Search from "../search/search.js";

const example = {
    toolbarLinks : [{href: "google.com", classNameMasterText:"search-toolbar-master-text", masterText: "Видео", type: customizedLinkTypes.MasterTextLink},
                    {href: "google1.com", classNameMasterText:"search-toolbar-master-text", masterText: "Картинки", type: customizedLinkTypes.MasterTextLink},
                    {href: "google1.com", classNameMasterText:"search-toolbar-master-text", masterText: "Новости", type: customizedLinkTypes.MasterTextLink},
                    {href: "google1.com", classNameMasterText:"search-toolbar-master-text", masterText: "Карты", type: customizedLinkTypes.MasterTextLink},
                    {href: "google1.com", classNameMasterText:"search-toolbar-master-text", masterText: "Маркет", type: customizedLinkTypes.MasterTextLink},
                    {href: "google1.com", classNameMasterText:"search-toolbar-master-text", masterText: "Еще...", type: customizedLinkTypes.MasterTextLink}],
    
    bottomLinks : [{href: "google.com", classNameMasterText:"search-master-text", classNameSecondaryText:"search-secondary-text", masterText: "Найдется все. Например, ", secondaryText:"фаза луны сегодня", type: customizedLinkTypes.MasterSecondaryTextLink}]
}


const SearchArea = ({data = example}) => {

    return (
        <div className="search-area">
            <CustomizedList className="search-toolbar" items = {data.toolbarLinks}/>
            <Search/>
            <CustomizedList className="search-bottom" items = {data.bottomLinks}/>
        </div>
    );
    
}

export default SearchArea;