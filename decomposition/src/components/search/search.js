import react, { useState } from "react";
import "./search.css";


// Поисковик
const Search = () =>{

    const [searchText, setSearchText] = useState("");

    const handleSearchChange = ({target}) => {
        setSearchText(target.value);
    }

    const handleSubmit = (evt) => {
        // Что-то ищем
    };


    return (
        <form onSubmit={handleSubmit}>
            <span className="yandex-text-first">Я</span><span className="yandex-text-last">ндекс</span>
            <input className="searcher" type="search" area-aria-label="Search" value={searchText} onChange={handleSearchChange}></input>
            <button className="searcher-button" type="submit">Search</button>
        </form>
    );

};

export default Search;