import react from "react";
import CustomizedList from "../customized/customized-list/customized-list.js"
import customizedLinkTypes from "../customized/customized-link/customized-link-types.js"
import "./bottom-area.css"

const example = [
                    {toolbarLinks: [{href: "weather.com", classNameMasterText:"bottom-toolbar-master-text", masterText: "Погода", type: customizedLinkTypes.MasterTextLink}],
                    bodyLinks: [{href: "weather.com/spb", classNameMasterText:"bottom-master-text", classNameSecondaryText:"bottom-secondary-text", imageSrc: "https://img.icons8.com/nolan/64/rain.png", masterText:"+17", secondaryText: "Утром +17, днем +20", type: customizedLinkTypes.MasterSecondaryImageLink}]},
                    
                    {toolbarLinks: [{href: "map.germany.com",classNameMasterText:"bottom-toolbar-master-text", masterText: "Карта Германии", type: customizedLinkTypes.MasterTextLink}],
                    bodyLinks: [{href: "schedule.germany.com", classNameMasterText:"bottom-master-text", masterText:"Расписания", type: customizedLinkTypes.MasterTextLink}]},
                    
                    {toolbarLinks: [{href: "efir.com", classNameMasterText:"bottom-toolbar-master-text", masterText: "Эфир", type: customizedLinkTypes.MasterTextLink}],
                    bodyLinks: [{href: "efir1.efir.com", classNameMasterText:"bottom-master-text", classNameSecondaryText:"bottom-secondary-text", imageSrc: "", masterText:"Управление как исскуство", secondaryText:"Успех", type: customizedLinkTypes.MasterSecondaryImageLink},
                                {href: "efir2.efir.com", classNameMasterText:"bottom-master-text", classNameSecondaryText:"bottom-secondary-text", imageSrc: "", masterText:"Ночь. Мир в это время", secondaryText:"earth TV", type: customizedLinkTypes.MasterSecondaryImageLink},
                                {href: "efir3.efir.com", classNameMasterText:"bottom-master-text", classNameSecondaryText:"bottom-secondary-text", imageSrc: "", masterText:"Андрей Возн...", secondaryText:"Совершенно секретно", type: customizedLinkTypes.MasterSecondaryImageLink}]}
                    
                ];



// Выводит область подвала - Погода, карта Германии и т.д.
const BottomArea = ({data = example}) => {
    
    const showBottom = () => {
        
        return (
            data.map((item,index) => <div className="bottom-item" key={index}>
                                        <CustomizedList className="bottom-toolbar" items = {item.toolbarLinks}/>
                                        <CustomizedList className="bottom-body" items = {item.bodyLinks}/>
                                    </div>)
        );
           
    }


    return (
        <div className="bottom-area">
            {showBottom()}
        </div>
    );
}

export default BottomArea;