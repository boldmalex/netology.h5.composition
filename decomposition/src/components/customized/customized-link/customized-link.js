import react from "react";
import propTypes from "prop-types";
import customizedLinkTypes from "./customized-link-types";


// Формирует элементы-ссылки в соответствии с передаными параметрами
const Customizedlink = (props) => {

    const {item} = props;

    // Отображает простую текстовую ссылку с master текстом
    const showMasterTextLink = (item) => {
        return (
            <a href = {item.href}>
                <span className={item.classNameMasterText}>{item.masterText}</span>
            </a>
        );
    }

    // Отображает текстовую ссылку как master + secondary текст
    const showMasterSecondaryTextLink = (item) => {
        return (
            <a href = {item.href}>
                <span className={item.classNameMasterText}>{item.masterText}</span> 
                &nbsp;
                <span className={item.classNameSecondaryText}>{item.secondaryText}</span> 
            </a>
        );
    }

    // Отображает ссылку как картинку с master текстом
    const showMasterImageLink = (item) => {
        return (
            <div>
                <a href = {item.href}>
                    <img src={item.imageSrc} alt={item.imageSrc}/>
                    <span className={item.classNameMasterText}>{item.masterText}</span> 
                </a>
            </div>
        );
    }

    // Отображает ссылку как картинку с master и secondary текстом
    const showMasterSecondaryImageLink = (item) => {
        return (
            <div>
                <a href = {item.href}>
                    <img src={item.imageSrc} alt={item.imageSrc}/>
                    <span className={item.classNameMasterText}>{item.masterText}</span> 
                    &nbsp;
                    <span className={item.classNameSecondaryText}>{item.secondaryText}</span>
                </a>
            </div>
        );
    }


    // Отображает элемент в зависимости от его типа
    const showLink = () => {
        switch(item.type) {
            case customizedLinkTypes.MasterTextLink :
                return showMasterTextLink(item);
            case customizedLinkTypes.MasterSecondaryTextLink :
                return showMasterSecondaryTextLink(item);
            case customizedLinkTypes.MasterImageLink :
                return showMasterImageLink(item);
            case customizedLinkTypes.MasterSecondaryImageLink:
                return showMasterSecondaryImageLink(item);
            default:
                return showMasterTextLink(item);
        }
    };

    return(
        <>
            {showLink()}
        </>
    );
}


Customizedlink.propTypes = {
    item : propTypes.shape({
        className: propTypes.string,
        classNameMasterText: propTypes.string,
        classNameSecondaryText: propTypes.string,
        imageSrc: propTypes.string,
        masterText: propTypes.string.isRequired,
        secondaryText: propTypes.string,
        href: propTypes.string.isRequired,
        type: propTypes.oneOf([customizedLinkTypes.MasterTextLink
                               , customizedLinkTypes.MasterSecondaryTextLink
                               , customizedLinkTypes.MasterImageLink
                               , customizedLinkTypes.MasterSecondaryImageLink]).isRequired
    })
}

export default Customizedlink;
