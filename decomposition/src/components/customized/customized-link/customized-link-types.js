
// Список возжных типов элементов-ссылок
const customizedLinkTypes = {
    MasterTextLink : 0,
    MasterSecondaryTextLink : 1,
    MasterImageLink: 2,
    MasterSecondaryImageLink: 3,
};

export default customizedLinkTypes;