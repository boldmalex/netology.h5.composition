import react from "react";
import Customizedlink from "../customized-link/customized-link.js"

// Выводит список кастомизированных линков
const CustomizedListItem = (props) => {
    return (
        <li className = {props.className}>
            <Customizedlink item = {props.item}/>
        </li>
    );
}

export default CustomizedListItem;