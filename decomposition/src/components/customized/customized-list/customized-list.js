import react from "react";
import propTypes from "prop-types";
import CustomizedListItem from "./customized-list-item";



// Выводит некий список переданных элементов
const CustomizedList = (props) => {

    const items = (items) =>{
        return (
            items.map((item,index)=><CustomizedListItem key={index} item = {item}/>)
        );
    };


    return (
        <div className = {props.className}>
            <ul>
                {items(props.items)}
            </ul>
        </div>
    );

}


export default CustomizedList;
