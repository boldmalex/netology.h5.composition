import react from "react";
import CustomizedList from "../customized/customized-list/customized-list.js"
import customizedLinkTypes from "../customized/customized-link/customized-link-types.js"
import "./news-area.css"

const example = {
        toolbarLinks : [{href: "google.com", classNameMasterText:"news-toolbar-master-text", masterText: "Сейчас в СМИ", type: customizedLinkTypes.MasterTextLink},
                        {href: "google1.com", classNameMasterText:"news-toolbar-master-text", masterText: "в Германии", type: customizedLinkTypes.MasterTextLink},
                        {href: "google1.com", classNameMasterText:"news-toolbar-master-text", masterText: "Рекомендуем", type: customizedLinkTypes.MasterTextLink}],

        bodyLinks : [{href: "google.com", classNameMasterText:"news-master-text", imageSrc: "https://img.icons8.com/officexs/16/000000/240-degrees.png", masterText: "Путин упростил получение автомобильных номеров", type: customizedLinkTypes.MasterImageLink},
                    {href: "google1.com", classNameMasterText:"news-master-text", imageSrc: "https://img.icons8.com/officexs/16/000000/240-degrees.png", masterText: "В команде Зеленского раскрыли план реформ на Украине", type: customizedLinkTypes.MasterImageLink},
                    {href: "google2.com", classNameMasterText:"news-master-text",imageSrc: "https://img.icons8.com/officexs/16/000000/240-degrees.png", masterText: "<Турпомощь> прокомментировала гибель десятков россиян в Анталье", type: customizedLinkTypes.MasterImageLink}],

        bottomLinks : [{href: "google.com", classNameMasterText:"news-master-text", classNameSecondaryText:"news-secondary-text", masterText: "USD MOEX 63,52", secondaryText:"+0,09", type: customizedLinkTypes.MasterSecondaryTextLink},
                        {href: "google1.com", classNameMasterText:"news-master-text", classNameSecondaryText:"news-secondary-text", masterText: "EUR MOEX 70,86", secondaryText:"+0,14", type: customizedLinkTypes.MasterSecondaryTextLink},
                        {href: "google2.com", classNameMasterText:"news-master-text", classNameSecondaryText:"news-secondary-text", masterText: "НЕФТЬ 64,90", secondaryText:"+1,63%", type: customizedLinkTypes.MasterSecondaryTextLink}]
}


// Выводит область новостей
const NewsArea = ({data = example}) => {

    return (
        <div className="news-area">
            <CustomizedList className="news-toolbar" items = {data.toolbarLinks}/>
            <CustomizedList className="news-body" items = {data.bodyLinks}/>
            <CustomizedList className="news-bottom" items = {data.bottomLinks}/>
        </div>
    );
}

export default NewsArea;