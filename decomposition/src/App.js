import react from "react";
import NewsArea from "./components/news-area/news-area";
import SearchArea from "./components/search-area/search-area";
import BottomArea from "./components/bottom-area/bottom-area";



function App() {
  return (
      <div>
        <NewsArea/>
        <SearchArea/>
        <BottomArea/>
      </div>
  );
}

export default App;
